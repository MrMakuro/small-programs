﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            Car car1 = new Car("Fiesta","Ford",4,"Blue");
            Console.WriteLine(car1.ToString());
            Console.WriteLine("Collection of Blue cars with four doors");

            Car car2 = new Car("Corolla", "Toyta", 4, "Blue");
            Car car3 = new Car("911", "Porche", 2, "Silver");
            Car car4 = new Car("Polo", "Volkswagen", 2, "Red");
            Car car5 = new Car("A4", "Audi", 4, "Yellow");
            Car car6 = new Car("Berlingo", "Citroën", 4, "Blue");
            Car car7 = new Car("Juke", "Nissan", 4, "Blue");
            ObservableCollection<Car> Cars = new ObservableCollection<Car>
            {
                car1,car2,car3,car4,car5,car6,car7
            };

            foreach (Car car in Cars){
                if (car.Colour == "Blue")
                    if (car.Doors == 4)
                        Console.WriteLine(car.ToString());
            }
            Console.ReadKey();
        }
    }
}
class Car
{
    public string Model { get; set; }
    public string Brand { get; set; }
    public int Doors { get; set; }
    public string Colour { get; set; }

    public Car(string model, string brand, int doors, string colour)
    {
        Model = model;
        Brand = brand;
        Doors = doors;
        Colour = colour;
    }

    public override string ToString()
    {
        return string.Format("Car: {0}, {1}, {2}, {3}", Model, Brand, Colour, Doors);  
    }

}
