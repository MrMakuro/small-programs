class Car:
    def __init__(self, model, brand, doors, colour):
        self.model = model
        self.brand = brand
        self.doors = doors
        self.colour = colour
    def __repr__(self):
        return str(self.__dict__)

car1 = Car("Fiesta", "Ford", 4, "Blue")
print(car1)
print("Collection of blue cars with four doors")
cars = []
cars.append( car1)
cars.append(Car ("Corolla", "Toyta", 4, "Blue"))
cars.append(Car ("911", "Porche", 2, "Silver"))
cars.append(Car ("Polo", "Volkswagen", 2, "Red"))
cars.append(Car ("A4", "Audi", 4, "Yellow"))
cars.append(Car ("Berlingo", "Citroën", 4, "Blue"))
cars.append(Car ("Juke", "Nissan", 4, "Blue"))

for car in cars:
    if car.colour == "Blue":
        if car.doors == 4:
            print(car)